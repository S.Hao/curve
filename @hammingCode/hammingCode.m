classdef hammingCode < code
    properties
    end
    
    methods
        
        function this = hammingCode(n)
            %hammingCode   Construct a binary Hamming Code object
            %
            %   C = hammingCode(n) creates a Hamming code
            %   object C with block length n.
            %
            %    n         CODE
            %    --------  -----------------------------
            %    2,3,4     repeat code
            %    7,15      (n,k) Hamming code
            %    8,16      (n,k) extended Hamming code
            %
            %   Brian Kurkoski 2015
            %
            
            if nargin < 1
                n = 7;
            end
            
            assert(n<17,'only n=2,3,7,8,15 or 16');
            %        1 2 3 4 5 6 7 8 9 0 1 2 3 4 15 16
            klist = [0 1 1 1 0 0 4 4 0 0 0 0 0 0 11 11];
            
            k = klist(n);
            m = n - k;
            assert(k ~= 0,'only n=2,3,7,8,15 or 16');
            
            if n == 2
                H = [1 1];
                G = [1 1];
            elseif (n==3) || (n==7) || (n==15)
                H = de2bi([n:-1:1],m)';
                [~,idx] = sort(sum(H,1));
                H = fliplr(H(:,idx ));
                A = H(:,1:k);
                G = [eye(k) , A'];
            elseif (n==4) || (n==8) || (n==16)
                H = de2bi([(n-1):-1:0],m-1)';
                [~,idx] = sort(sum(H,1));
                H = fliplr(H(:,idx ));
                s = sum(H);
                H = [H ; mod(ones(1,n)+s,2)];
                
                A = H(:,1:k);
                G = [eye(k) , A'];
            end
            
            this.H = H;
            this.G = G;
            [this.k , this.n] = size(G);
            
            C = zeros(this.n,2^this.k);
            for ii = 1:2^this.k
                u       = de2bi(ii-1,this.k);
                C(:,ii) = mod((u * this.G)',2);
            end
            %this.C = 2*C - 1;
            this.C = C;
        end
        
        function xhat = decode(this,y)
            dist = Inf;
            best = Inf;
            C = this.C;
            for ii =1:2^this.k
                t = norm(y - C(:,ii));
                if t < dist
                    dist = t;
                    best = ii;
                end
            end
            xhat = this.C(:,best);
        end
        
        
    end
end