%CURVE  Library for communication system simulations
%
%Manage a simulation that implements the CURVE program interface
%  curveGenerate      - Generate a data file from parameters
%  curveSimu          - Perform a simulation using the data file
%
%Implement a simulation function the CURVE program interface
%  curveEnter         - Initialize, called after entering the simulation function
%  curveDefaultValue  - Set default values for your simulation parameters
%  curveErrorLim      - Controls the simulation while loop
%  curveLeave         - Called before leaving the simulation function
%
%  curveAddField      - Parallel sims: combining parameters are added
%  curveEqualField.m  - Parallel sims: combining parameter must be equal
%  curveConcatField.m - Parallel sims: combining parameters are concatenated
%
%Misc functions 
%  curveSendMail      - Send an email, for example, when a simulation is finished
%  curveRemoteCommand - Execute a command on the remote server
%
%See curve.pdf for full documentation.  
%
%Brian Kurkoski, 2016
