function curve = curveDefaultValue(curve,field,defaultValue,option)
%   CURVE = curveDefaultValue(CURVE,FIELD,DEFAULTVALUE)
%
%   If CURVE has a non-empty FIELD, then CURVE is returned unchanged.
%   Otherwise, the returned CURVE has FIELD with DEFAULTVALUE
%
%   For parallel processing simulations, sets the combining mode for the
%   field.  This is required for parallel processing.  For 'add', this also
%   sets the values in CURVE(2), CURVE(3),... to 0.
%
%   CURVE = CURVEDEFAULTVALUE(CURVE,FIELD,DEFAULTVALUE,'add')
%   CURVE = CURVEDEFAULTVALUE(CURVE,FIELD,DEFAULTVALUE,'concat')
%
%   CURVEDEFAULTVALUE(CURVE,FIELD,DEFAULTVALUE,'force')  DEFAULTVALUE is 
%   unconditionally set in FIELD.
%
% (c) Brian Kurkoski
% Distributed under an MIT-like license; see the file LICENSE
%


%Add field if it does not exist, array form
if ~isfield(curve,field)
    [curve(:).(field)] = deal([]);
end

for ii = 1:length(curve)
    if isempty(getfield(curve(ii),field)) || (nargin > 3 && strcmp(option,'force'))
        curve(ii) = setfield(curve(ii),field,defaultValue);
    end
    
    if nargin > 3
        sys = curve(1).sys;
        switch option
            case 'add'
                if ~any( strcmp(sys.addField,field) )
                    sys.addField{end+1} = field;
                end
            case 'first'
                if ~any( strcmp(sys.firstField,field) )
                    sys.firstField{end+1} = field;
                end
            case 'equal'
                if ~any( strcmp(sys.equalField,field) )
                    sys.equalField{end+1} = field;
                end
            case 'concat'
                if ~any( strcmp(sys.concatField,field) )
                    sys.concatField{end+1} = field;
                end
            case 'otherwise'
                error('OPTION unrecognized');
        end
        
        for ii = 1:length(curve)
            curve(ii).sys = sys;
        end
    end
    
end

%for parallel simulations: curve(2), curve(3), ... are:
%   for addfields, set to 0
%   for concat fields, set to [] or {}
%

if nargin > 3
    switch option
        case 'add'
            for ii = 2:length(curve)
                curve(ii).(field) = 0;
            end
        case 'concat'
            if isnumeric(curve(1).(field))
                for ii = 2:length(curve)
                    curve(ii).(field) = [];
                end
            elseif iscell(curve(1).(field))
                for ii = 2:length(curve)
                    curve(ii).(field) = {};
                end
            end
    end
end

return
