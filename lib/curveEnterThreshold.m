function [curve,thresholdInitReturn] = curveEnterThreshold(curve,searchParam,ilow,ihigh,resolution)


%CURVEENTERTHRESHOLD  Additional initialization for the threshold search
%
%  Provides initializaitons for threshold searches, to be used after CURVEENTER
%
%  In typical use, the top of a threshold search funciton looks like this:
%
%    function mysimulation(varargin)
%
%      curve = curveEnter(mfilename,varargin);
%      curve = curveEnterThreshold(curve,'SNRdB',0,6,0.1)
%
%  This indicates the threshold search should be in the parameter 'SNRdB'
%  (which is a field of CURVE), the initial low search point is SNRdB = 0,
%  and the initial high search point is SNRdB.  The search should be
%  performed with a resolution 0.1, that is, the search is continued until
%  the gap between high and low is 0.1.
%
%  The function mysimulation should provide a field curve.check which is 0
%  for the initial low search point, and is 1 for the initial high search
%  point.
%
%  Brian Kurkoski


narginchk(5,5)
assert(isa(curve,'struct'),'first input is not a struct');
assert(isa(searchParam,'char'),'second input is not a character array');

if ~isfield(curve,'searchParam')
    curve.searchParam = searchParam;
end

if ~isfield(curve,'ilow')
    curve.ilow = ilow;
end

if ~isfield(curve,'ihigh')
    curve.ihigh = ihigh;
end

if ~isfield(curve,'resolution')
    curve.resolution = resolution;
end

if isfield(curve,'thresholdInitReturn')
    thresholdInitReturn = curve.thresholdInitReturn;
else
    thresholdInitReturn = false;
end