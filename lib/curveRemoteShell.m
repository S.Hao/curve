function result = curveRemoteShell(varargin)
%curveSimuRemote - Run a shell command on the remote server
%
%  Example: 
%      curveSimuRemote('pwd')
%      curveSimuRemote('qstat -u username')
%
%  If the command consists of literal strings then:
%      curveSimuRemote qstat -u username
%  is equivalent to the the second commmand above.
%
%  Brian Kurkoski
%

if nargin < 1
    error('requires one input argument')
else
    inputString = '';
    for ii = 1:length(varargin)-1
        inputString = [inputString varargin{ii} ' '];
    end
    inputString = [inputString varargin{end}];
end

[config,result,pbsdir,configurationFileName] = curveRemoteLoadConfiguration;
if ~result
    fprintf('Failed to load configuration file: %s/%s must be a configuration file\n',pbsdir,configurationFileName);
    return
end

if ~ispc
    cmd = sprintf('ssh %s@%s "cd %s ; %s"',config.username,config.servername,config.remoteDir,inputString);

    [status,result] = system(cmd);
    if status
        fprintf('Command had no output, or command failed\n');
    end
else
    error('Windows version not yet tested');
end

if nargout == 0
    fprintf('%s',result);
    clear result;
end

return