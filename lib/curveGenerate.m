function curveGenerate(filename,functionName,varargin)
% CURVEGENERATE Generates a simulation file from input arguments
%
%  CURVEGENERATE('filename','simulationfunction')
%  Creates a new simulation data file in FILENAME.MAT, using the
%  the simulation function 'simulationfunction'.  If FILENAME.MAT already exists,
%  you will be prompted before overwriting.  The function specified by
%  'simulationfunction' must follow the program interface, see CURVEENTER.
%
%  CURVEGENERATE('filename','functionname','field1',value1,...)
%  Additionally sets the specified field-value pairs
%
%  CURVEGENERATE('filename','functionname','field1',{val1 val2 val3},...)
%  Creates an array of CURVE structres, length the same as the cell array,
%  where field values are set to val1, val2, val3, ...
%
%  Brian Kurkoski 2005, 2016
%

if nargin < 2
    error('minimum two input arguments')
end

curve = struct(varargin{:});

if ~exist(functionName,'file')
    warning('Function %s does not exist', upper(functionName));
end
curve = curveInit(curve,functionName,varargin);
%curve(1,1).sys.inputs = varargin;

%Check for local filename before overwriting
%The EXIST command also searches the path, not the desired behavior.
x1 = dir([filename '.mat']);
if length(x1) == 0
    r = 'y';
else
    fprintf('WARNING: About to overwrite %s.MAT. ',upper(filename));
    r = input('Enter ''y'' to overwrite existing file [y/N] ','s');
end

if length(r) > 0 & strcmpi(r(1),'y')
    save(filename,'curve');
    fprintf('Saved to file %s.MAT\n',upper(filename));
else
    fprintf('Did nothing.\n');
    return;
end


