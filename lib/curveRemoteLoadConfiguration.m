function [config,result,pbsdir,configurationFileName] = curveRemoteLoadConfiguration
%curveRemoteLoadConfiguration - Load configuration file for remote servers
%
%   This is an internal function called by the Curve Library.
%

%default path and configuration file name
configurationFileName = 'configureRemote.m';
pbsdir = 'var';



d = dir;
t = strcmp({d.name},pbsdir);
result = true;
if ~any(t)
    config = struct;
    result = false;
    %fprintf('Error: a %s directory containing %s is required\n',upper(pbsdir),configurationFileName);
    return;
end

%assert(any(t),'Error: a %s directory containing %s is required',upper(pbsdir),configurationFileName);
t = find(t,1);
assert(d(t).isdir,'Error: %s must be a directory, not a file',upper(pbsdir));

d = dir([pbsdir '/' configurationFileName]);
assert(length(d) > 0, 'Directory %s/ must contain file %s.M',upper(pbsdir),upper(configurationFileName));
assert(strcmp( d(1).name, configurationFileName), 'Directory %s must contain file %s.M',upper(pbsdir),upper(configurationFileName));

tdir = [pwd '/' pbsdir];
addpath(tdir);
config = configureRemote;
rmpath(tdir);
