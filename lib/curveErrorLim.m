function c = curveErrorLim(curve,skiptest)
%  C = CURVEERRORLIM(CURVE)
%
%  C is TRUE if a simulation should be continued, and FALSE if the simuation
%  should be stopped.
%
%  If the current work interval is exceeded, then C is FALSE.  Otherwise, the
%  text string in CURVE.CONTINUE is evaluated, and is returned.
%
%  Set workInterval equal to 0 for no work interval test.
%
%  Typical usage:
%
%  while(curveErrorLim(curve))
%    ... run a simulation ...
%  end
%
%  Brian Kurkoski 7/2002,5/2005,11/2006,2/2016
%

if nargin < 2
    skiptest = false;
end

%SKIPTEST=TRUE means we are calling from curveSimu. Skip the tests for the
%existence of the continue field (because it may not be set yet) and for
%the workInterval (not releavant in this context).
if (~skiptest)
    if ~isfield(curve,'continue')
        error('CURVE must contain field ''continue''');
    end
    
    if isempty(curve.continue)
        error('curve.continue must be a string or logical value');
    end
    
    if curve.sys.workInterval > 0
        timeSoFar = etime(clock,curve.sys.startTime);
        if timeSoFar > curve.sys.workInterval
            c = false;
            fprintf('Work interval %d reached ',curve.sys.workInterval);
            fprintf('(with elapsed time=%0.1f)\n',timeSoFar);
            return
        end
    end
else
    %if curve.continue is not specified, then force continue
    if ~isfield(curve,'continue')
        curve.continue = true;
    elseif isempty(curve.continue)
        curve.continue = true;
    end
end

if skiptest && curve.sys.elapsedTime == 0
    %applies when user specified 'continue' in curveGenerate, but 
    %eval(curve.continue) will fail because not all variables specified yet
    %this only occurs the first time, checking using elapsedTime == 0
    c = true;
    
elseif ischar(curve.continue)
    %case curve.continue is a character string, evaluate the string
    c = eval(curve.continue);
    
elseif islogical(curve.continue) ||  length(curve.continue) == 1
    %case curve.continue is numeric, probably 0 or 1
    c = curve.continue(1);

else
    error('curve.continue must be character or numeric')
end

if ~islogical(c) && ~isnumeric(c)
    error('curve.continue must evaluate to a logical or numeric expression');
end


return;