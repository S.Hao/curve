function curve = curveLeave(curveArray)
%CURVE = curveLeave(CURVEARRAY)
%
%  Performs clean up functions before leaving a simulation function, also
%  combines the results of multiple simulations after running on parallel
%  processors.  CURVEARRAY is are the results of multiple parallel
%  simulations, and CURVE is the combined result.
%
%  CURVE.SYS contains the fields ADDFIELD and CONCATFIELD
%  which are arrays of strings containing the names of fields.  Fields are 
%  combined according to the following rules.
%    * For the fields in ADDFIELD, the values must be numerical, and will be added.
%    * For the fields in CONCATFIELD, the values should be row vectors, and will
%      be concatenated.
%    * For any other field, the value in CURVEARRAY(1) is copied to 
%      CURVE, and remainding values in CURVEARRAY are ignored.
%
%  If any element of CURVEARRAY does not contain an ETIME field then it 
%  is skipped.  This allows skipping simulations that were not attempted.
%
%  Brian Kurkoski
%


if length(curveArray) == 1
    %non-parallel
    curve = curveArray;
else
    %parallel
    sys = curveArray(1).sys;
    curve = local_curveCombineParallel(sys,curveArray(1),curveArray(2));
    for ii = 3:length(curveArray)
        curve = local_curveCombineParallel(sys,curve, curveArray(ii) );
    end
    
    curve = orderfields(curve,curveArray(1));
end

curve.sys.elapsedTime = curve.sys.elapsedTime + etime(clock,curve.sys.startTime);
curve.sys.startTime   = [];

end


function curve = local_curveCombineParallel(sys,c1,c2)

%unless a field is 'add' or 'concat' then copy the field from C1, ignore C2
c = c1;

addField    = sys.addField;
concatField = sys.concatField;

for ii = 1:length(addField)
    if isfield(c1,addField{ii})
        t = getfield(c1,addField{ii}) + getfield(c2,addField{ii});
        c = setfield(c,addField{ii},t);
    else
        error('Post-parallel combining field %s does not exist in CURVE.',upper(addField{ii}));
    end 
end

for ii = 1:length(concatField)
    if isfield(c1,concatField{ii})
        t = [getfield(c1,concatField{ii}) , getfield(c2,concatField{ii})];
        c = setfield(c,concatField{ii},t);
    else
        error('Post-parallel combining field %s does not exist in CURVE.',upper(concatField{ii}));
    end
end

curve = c;

return





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%earlier versions would not combine any field that had no etime
%if ~isfield(c2,'etime')
%     curve = c1;
%     return;
% end
% 
% if ~isfield(c1,'etime')
%     curve = c2;
%     return;
% end



c = struct;

%equalField = sys.equalField;
addField = sys.addField;
concatField = sys.concatField;
%firstField = sys.firstField;

equalField{end+1} = 'sys';
equalField{end+1} = 'continue';

if false
    %check for missing fields in C1
    fieldList = fields(c1);
    for ii = 1:length(fieldList)
        s = fieldList{ii};
        if (~any(strcmp(s,equalField))) && (~any(strcmp(s,addField))) && (~any(strcmp(s,concatField))) && (~any(strcmp(s,firstField)))
            error('ERROR: CURVE has field %s, but no post-parallel combination method was given\n',upper(s));
        end
    end
    
    %check for missing fields in C2
    fieldList = fields(c2);
    for ii = 1:length(fieldList)
        s = fieldList{ii};
        if (~any(strcmp(s,equalField))) && (~any(strcmp(s,addField))) && (~any(strcmp(s,concatField))) && (~any(strcmp(s,firstField)))
            error('ERROR: CURVE has field %s, but no post-parallel combination method was given\n',upper(s));
        end
    end
end

%if the above test pass, then start combining

%The following fields must be equal
for ii = 1:length(equalField)
    c = local_equal(c,c1,c2,equalField{ii});
end


for ii = 1:length(addField)
    if isfield(c1,addField{ii})
        t = getfield(c1,addField{ii}) + getfield(c2,addField{ii});
        c = setfield(c,addField{ii},t);
    else
        error('Post-parallel combining field %s does not exist in CURVE.',upper(addField{ii}));
    end 
end



for ii = 1:length(concatField)
    if isfield(c1,concatField{ii})
        t = [getfield(c1,concatField{ii}) , getfield(c2,concatField{ii})];
        c = setfield(c,concatField{ii},t);
    else
        error('Postparallel combining field %s does not exist in CURVE.',upper(concatField{ii}));
    end
end



for ii = 1:length(firstField)
    if isfield(c1,firstField{ii})
        t = getfield(c1,firstField{ii});
        c = setfield(c,firstField{ii},t);
    else
        error('Postparallel combining field %s does not exist in CURVE.',upper(firstField{ii}));        
    end
end

curve = c;

end


function c = local_equal(c,c1,c2,fieldname)
%compares if c1 and c2 have equal values in fieldname, and if they do,
%add it to the structure c.

if isfield(c1,fieldname) && isfield(c2,fieldname)
    a = getfield(c1,fieldname);
    b = getfield(c2,fieldname);
    
    if ischar(a)
        t = strcmp(a,b);
    elseif isnumeric(a)
        t = all(all(a == b));
    elseif isstruct(a)
        %do not compare struct
        t = true;
    else
        error('unknown field type');
    end
    
    if t == 1
        %if isnan(a); keyboard;end
        c = setfield(c,fieldname,a);
        return;
    else
        fprintf('Value of field %s do not agree\n',fieldname);
        error('error');
    end
    
else
    error('Postparallel combining field %s does not exist in CURVE.',upper(fieldname));
end

end


