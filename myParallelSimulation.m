function curve = myParallelSimulation(varargin)

curve = curveEnter(mfilename,varargin);

%sets how to handle parallel simulation
curve = curveAddField(curve,'nBitError','nWordError','nWords');
curve = curveEqualField(curve,'Pe','n');

%Input parameters: default values
curve = curveDefaultValue(curve,'Pe',0.1,'input');
curve = curveDefaultValue(curve,'n',7,'input');

%Output parameters: initalize to 0
curve = curveDefaultValue(curve,'nBitError',0,'add');
curve = curveDefaultValue(curve,'nWordError',0,'add');
curve = curveDefaultValue(curve,'nWords',0,'add');

%Stopping condition: stop when 10 word errors accummulated
curve = curveDefaultValue(curve,'continue','curve.nWordError <10');

%setup: create Hamming Code object
n  = curve.n;
HC = hammingCode(n);

keyboard

%SIMULATION CORE
parfor worker = 1:length(curve);
    while (curveErrorLim( curve(worker) ))
        %encoder
        u = randi(2,HC.k,1) - 1 ;
        x = mod(HC.G' * u, 2);
        
        %noise
        z = (rand(n,1) < curve(worker).Pe) + 0;
        y = mod(x+z,2);
        
        %decoder
        xhat = HC.decode(y);
        
        %count and accumulate errors
        ne = length(find(xhat ~= x));
        curve(worker).nBitError  = curve(worker).nBitError + ne;
        curve(worker).nWordError = curve(worker).nWordError + (ne > 0);
        curve(worker).nWords     = curve(worker).nWords + 1;
    end
end


curve = curveLeave(curve);

%Convenience calculations
curve.ber = curve.nBitError / (curve.nWords * curve.n);
curve.wer = curve.nWordError / curve.nWords ;




