classdef code
    properties
        G           %Generator matrix
        H           %Parity check matrix
        k           %dimension
        n           %block length
        C           %codebook: n by 2^k
    end
    
    methods
        
        function this = code(G,H)
            %code   Construct an (error-correcting) code object
            %
            %   C = code(G) creates a code object C using generator
            %   matrix G.  Generator vectors are in columns.
            %
            %   L = code([],H) creates a code object C using the check
            %   matrix H.
            %
            %   Brian Kurkoski 2015
            %
            
            if nargin > 0
                if length(G) > 0
                    this.G = G;
                    [n,k] = size(G);
                    this.n = max(n,k);
                    this.k = min(n,k);
                end
            end
            
            if nargin > 1
                if length(H) > 0
                    this.H = H;
                    [n,m] = size(H);
                    this.n = max(n,m);
                    m      = min(n,m)
                    this.k = this.n - m;
                end
            end
            
        end
    end
end